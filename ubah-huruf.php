<?php
function ubah_huruf($string){
    $str = "";
    for($i = 0; $i < strlen($string); $i++){
        if($string[$i] === 'z'){
            $str .= 'a';
        }else {
            $num = ord($string[$i]);
            $str .= chr($num + 1);
        }
    }
    return $str . '<br>';
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>